FROM python:3.8-alpine
RUN apk update
RUN apk add --no-cache build-base jpeg-dev zlib-dev
ENV LIBRARY_PATH=/lib:/usr/lib
ENV PYTHONUNBUFFERED 1

WORKDIR /app

COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

COPY /images /app/images

COPY /src /app/src

WORKDIR /app/src

CMD [ "python", "./server.py" ]
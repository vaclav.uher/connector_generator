from PIL import Image, ImageDraw
import random
from image_generator.tools import register


@register
def draw_circles(x_count: int=3, y_count: int=2,
                 circles_size: int=5, circles_color: str='green,yellow', circle_outline_color: str=None,
                 image_width: int=200, image_height: int=200):
    """
    Generates image with circles. If list of colors is defined, each circle has randomly chosen color.
    :param x_count: number of circle columns
    :param y_count: number of circle rows
    :param circles_size: size of a circle
    :param circles_color: fill color, can be single value (name, hex value), tuple, set or list.
    :param circle_outline_color: border color
    :param image_width: resultant image width
    :param image_height: resultant image height
    :return: generated @Image
    """
    x_count = int(x_count)
    y_count = int(y_count)
    circles_size = int(circles_size)
    image_width = int(image_width)
    image_height = int(image_height)

    image = Image.new('RGBA', (image_width, image_height))
    draw = ImageDraw.Draw(image)
    # convert single color to array
    circles_color = circles_color.split(',')
    # calculate spacing between circles
    spacing_x = int((image_width - (x_count * circles_size)) / (x_count + 1))
    spacing_y = int((image_height - (y_count * circles_size)) / (y_count + 1))
    for x in range(spacing_x, image_width - spacing_x, spacing_x + circles_size):
        for y in range(spacing_y, image_height - spacing_y, spacing_y + circles_size):
            draw.ellipse((x, y, x + circles_size, y + circles_size),
                         fill=random.choice(circles_color),
                         outline=circle_outline_color)
    return image


@register
def draw_rectangle(x_count: int=2, y_count: int=2,
                   rect_width: int=5, rect_height: int=5, rect_color: str='green,orange',
                   rect_outline_color: str=None, image_width: int=200, image_height: int=200):
    """

    :param x_count: number of rectangle columns
    :param y_count: number of rectangle rows
    :param rect_width: rectangle x dimension
    :param rect_height: rectangle y dimension
    :param rect_color:  fill color of rectangle, can be single value (name, hex value), tuple, set or list.
    :param rect_outline_color: border color
    :param image_width: resultant image width
    :param image_height: resultant image height
    :return: generated @Image
    """
    x_count = int(x_count)
    y_count = int(y_count)
    rect_width = int(rect_width)
    rect_height = int(rect_height)
    image_height = int(image_height)

    image = Image.new('RGBA', (image_width, image_height))
    draw = ImageDraw.Draw(image)
    # convert single color to array
    rect_color = rect_color.split(',')
    # calculate spacing between rectangles
    spacing_x = int((image_width - (x_count * rect_width)) / (x_count + 1))
    spacing_y = int((image_height - (y_count * rect_height)) / (y_count + 1))
    for x in range(spacing_x, image_width - spacing_x, spacing_x + rect_width):
        for y in range(spacing_y, image_height - spacing_y, spacing_y + rect_height):
            draw.rectangle((x, y, x + rect_width, y + rect_height),
                           fill=random.choice(rect_color),
                           outline=rect_outline_color)
    return image


if __name__ == '__main__':
    draw_circles(5, 2, 20, 'green,yellow,red', 'black', 400, 200).show()
#   draw_rectangle(5, 2, 20, 15, 'green,orange,yellow, 'black', 200, 200).show()

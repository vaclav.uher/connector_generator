import functools
import time

AVAILABLE_FUNCTIONS = {}


def timer(func):
    """Print the runtime of the decorated function"""

    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()  # 1
        value = func(*args, **kwargs)
        end_time = time.perf_counter()  # 2
        run_time = end_time - start_time  # 3
        # TODO to log
        print("Finished {} in {:.2f} ms".format(func.__name__, run_time * 1000))
        return value

    return wrapper_timer


def register(func):
    """Register a function for requests usage."""
    AVAILABLE_FUNCTIONS[func.__name__] = func
    return func

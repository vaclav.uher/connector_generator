import glob
import inspect
import logging
from functools import lru_cache, wraps
from os.path import abspath, join, split, splitext

import numpy as np
from PIL import Image
from PIL.ImageColor import colormap, getrgb
from frozendict import frozendict


from image_generator.tools import timer, AVAILABLE_FUNCTIONS

IMAGE_FOLDER = '../images'
ROTATE = 'rotate'
BACKGROUND = 'background'
TRANSPARENT_FILE = 'transparent.png'
TRANSPARENT = 0
OPAQUE = 255
TRANSPARENT_COLOR = (0, 0, 0, TRANSPARENT)
BLACK = (0, 0, 0, OPAQUE)
SPECIAL = 'special'


def freeze_args(func):
    """Transform mutable dictionary
    Into immutable
    Useful to be compatible with cache
    """

    @wraps(func)
    def wrapped(*args, **kwargs):
        args = tuple([frozendict(arg) if isinstance(arg, dict) else arg for arg in args])
        kwargs = {k: frozendict(v) if isinstance(v, dict) else v for k, v in kwargs.items()}
        return func(*args, **kwargs)

    return wrapped


@timer
@freeze_args
@lru_cache(maxsize=128)
def retrieve_image(file_name, args):
    args = dict(args)
    if SPECIAL in args:
        return call_generator_function(file_name, args)
    background = np.copy(load_image(TRANSPARENT_FILE))
    rotate = int(args.pop(ROTATE, 0))

    for key in sorted(args):
        try:
            mask = load_image(key + '.png')
        except IOError:
            continue
        color = parse_color(args.get(key, 'black'))
        if key == BACKGROUND:
            background = add_mask(background, background, color, TRANSPARENT)
        else:
            background = add_mask(background, mask, color, OPAQUE)

    image = Image.fromarray(background)
    image = image.rotate(rotate)
    image.filename = file_name
    return image


def parse_color(color):
    print(color)
    if not color:
        return TRANSPARENT_COLOR
    rgb = getrgb(color)
    try:
        if len(rgb) == 3:
            rgb = rgb + (OPAQUE,)
        return rgb
    except ValueError:
        return TRANSPARENT_COLOR





def add_mask(background, mask, color, alpha_value):
    if not color:
        color = BLACK
    red, green, blue, alpha = mask.T  # Temporarily unpack the bands for readability

    # Replace selected alpha with color...
    to_copy = alpha == alpha_value
    background[..., :][to_copy.T] = color
    return background


@lru_cache(maxsize=32)
def load_image(name):
    path = abspath(join(IMAGE_FOLDER, name))
    logging.info('Loading image {}'.format(path))
    try:
        image = Image.open(path)
    except IOError as e:
        # TODO log error
        print(e)
        raise e
    image_array = np.array(image)

    return image_array


def call_generator_function(file_name, args):
    special = args.pop(SPECIAL)
    method = AVAILABLE_FUNCTIONS[special]
    try:
        image = method(*[], **args)
    except TypeError as e:
        # TODO log error
        print(e)
        image = Image.fromarray(load_image('cross.png'))

    image.filename = file_name
    return image


def list_connectors():
    files = glob.glob(join(IMAGE_FOLDER, '*.png'))
    images = []
    for file in files:
        images.append(splitext(split(file)[-1])[0])
    return images


def list_functions():
    functions = []
    for name in AVAILABLE_FUNCTIONS:
        specs = inspect.getfullargspec(AVAILABLE_FUNCTIONS[name])
        functions.append({'name': name, 'params': specs.annotations, 'values': specs.defaults})
    return functions


if __name__ == '__main__':
    print(list_functions())

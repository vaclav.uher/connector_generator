from flask import Flask
from flask import request, send_file, render_template
from image_generator import image_operations, tools
import io
import json


# logger = logging.basicConfig(format='%(asctime)s %(message)s', filename='/var/log/dongfla.log', level=logging.INFO)
# logger.info('Starting server.')

app = Flask(__name__)


@app.route('/', methods=['GET'])
@tools.timer
def welcome_page():
    return render_template('index.html')


@app.route('/available_connectors.json')
@tools.timer
def list_images():
    connectors = {'images': image_operations.list_connectors(),
                  'functions': image_operations.list_functions()
                  }
    return json.dumps(connectors)


# http://0.0.0.0:5000/image/connector.png?ethernet&ethernet_m1=green&ethernet_m2=blue&rotate=180&background=silver

@app.route('/image/<image_name>', methods=['GET'])
@tools.timer
def get_image(image_name):
    im = image_operations.retrieve_image(image_name, request.args)
    img_io = io.BytesIO()
    im.save(img_io, 'PNG')
    img_io.seek(0)
    return send_file(img_io, attachment_filename=image_name, mimetype='image/png')


if __name__ == '__main__':
    host = '0.0.0.0'
    app.run(host=host)

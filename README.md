# connector_generator

A Flask based web service for generating color variants images of network connectors (like RJ45) and other components (like LEDs) for designing rack, switches, etc.
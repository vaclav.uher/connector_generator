import unittest
import numpy as np
from image_generator import image_operations


class TestImageOperations(unittest.TestCase):
    def test_load_image(self):
        """
        Testing image loader
        """
        try:
            result = image_operations.load_image('doesnotexist')
        except Exception as e:
            self.assertIsInstance(e, IOError)

        result = image_operations.load_image('ethernet.png')
        self.assertIsInstance(result, np.ndarray)
        np.testing.assert_array_equal([0, 0, 0, 0], result[0, 0, :])
        np.testing.assert_array_equal([0, 0, 0, 255], result[86, 41, :])

    def test_parse_color(self):
        colors = {'nonsense': [0, 0, 0, 0],
                  'white': [255, 255, 255, 255],
                  'black': [0, 0, 0, 255],
                  'red': [255, 0, 0, 255],
                  'lime': [0, 255, 0, 255],
                  'green': [0, 128, 0, 255],
                  'blue': [0, 0, 255, 255],
                  '#fa8072': [250, 128, 114, 255],
                  }

        for color in colors:
            np.testing.assert_array_equal(image_operations.parse_color(color), colors[color])
        np.testing.assert_array_equal(image_operations.parse_color(None), [0, 0, 0, 0])


if __name__ == '__main__':
    unittest.main()
